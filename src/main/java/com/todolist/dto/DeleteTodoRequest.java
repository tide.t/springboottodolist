package com.todolist.dto;

public class DeleteTodoRequest {
	private int id;

	public DeleteTodoRequest() {
	}

	public DeleteTodoRequest(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
