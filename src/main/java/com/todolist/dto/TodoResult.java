package com.todolist.dto;

public class TodoResult {
	private int status;
	private String statusDescription;
	private TodoDTO todo;

	public TodoResult() {
	}

	public TodoResult(int status, String statusDescription) {
		this.status = status;
		this.statusDescription = statusDescription;
	}

	public TodoResult(int status, String statusDescription, TodoDTO todo) {
		this.status = status;
		this.statusDescription = statusDescription;
		this.todo = todo;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public TodoDTO getTodo() {
		return todo;
	}

	public void setTodo(TodoDTO todo) {
		this.todo = todo;
	}

}
