package com.todolist.service;

import java.util.List;
import com.todolist.dto.TodoDTO;
import com.todolist.dto.TodoResult;

public interface TodoService {
	List<TodoDTO> getAll();

	TodoResult add(String todoname);

	TodoResult update(TodoDTO todo);

	TodoResult delete(int id);

	TodoResult delete_all();
}
