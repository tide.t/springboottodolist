package com.todolist.service.Impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.todolist.dto.TodoDTO;
import com.todolist.dto.TodoResult;

import com.todolist.entity.TodoEntity;
import com.todolist.mapper.TodoMapper;
import com.todolist.repository.TodoRepository;
import com.todolist.service.TodoService;

@Service
public class TodoServiceImpl implements TodoService {
	private final TodoRepository todoRepository;
	private final TodoMapper todoMapper;

	@Autowired
	public TodoServiceImpl(TodoRepository todoRepository, TodoMapper todoMapper) {
		this.todoRepository = todoRepository;
		this.todoMapper = todoMapper;
	}

	@Override
	public List<TodoDTO> getAll() {
		List<TodoEntity> listEntity = todoRepository.findAll();
		List<TodoDTO> listDTO = new ArrayList<TodoDTO>();
		
		for (TodoEntity entity : listEntity) {
			TodoDTO dto = todoMapper.toDTO(entity);
			listDTO.add(dto);
		}
		
		return listDTO;
	}

	@Override
	public TodoResult add(String name) {
		TodoResult result = new TodoResult();
		TodoEntity entity = new TodoEntity(name, 1);
		
		try {
			TodoEntity result_entity = todoRepository.save(entity);
			TodoDTO todoDTO = todoMapper.toDTO(result_entity);
			result = new TodoResult(1, "Add new todo success", todoDTO);
		} catch (Exception e) {
			result = new TodoResult(0, e.getMessage());
		}

		return result;
	}

	@Override
	public TodoResult update(TodoDTO todo) {
		TodoResult result = new TodoResult();
		
		try {
			TodoEntity entity = todoMapper.toEntity(todo);
			todoRepository.save(entity);
			result = new TodoResult(1, "Update todo success");

		} catch (Exception e) {
			result = new TodoResult(0, e.getMessage());
		}

		return result;
	}

	@Override
	public TodoResult delete(int id) {
		TodoResult result = new TodoResult();
		
		try {
			todoRepository.delete(id);
			result = new TodoResult(1, "Delete todo success");
		} catch (Exception e) {
			result = new TodoResult(0, e.getMessage());
		}

		return result;
	}

	@Override
	public TodoResult delete_all() {
		TodoResult result = new TodoResult();
		
		try {
			todoRepository.deleteAll();
			result = new TodoResult(1, "Delete all todo success");
		} catch (Exception e) {
			result = new TodoResult(0, e.getMessage());
		}

		return result;
	}
}
