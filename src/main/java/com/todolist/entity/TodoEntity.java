package com.todolist.entity;

import java.util.Date;

import javax.persistence.*;
import lombok.Data;


@Data
@Entity
@Table(name = "todo")
public class TodoEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "status")
	private int status;	
	
	private Date date_create;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public TodoEntity() {
	}

	public TodoEntity(int id, String name, int status) {
		this.id = id;
		this.name = name;
		this.status = status;
	}

	public TodoEntity(String name, int status) {
		this.name=name;
		this.status=status;
	}

}
