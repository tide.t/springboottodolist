package com.todolist.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.todolist.dto.TodoResult;
import com.todolist.dto.AddTodoRequest;
import com.todolist.dto.TodoDTO;
import com.todolist.service.TodoService;

@RestController
@RequestMapping("/todo")
public class TodoAPI {

	private final TodoService todoService;

	@Autowired
	public TodoAPI(TodoService todoService) {
		this.todoService = todoService;
	}

	@CrossOrigin
	@GetMapping("/")
	public List<TodoDTO> getTodoList() {
		return todoService.getAll();
	}

	@CrossOrigin
	@PostMapping("/add")
	public TodoResult addTodo(@RequestBody AddTodoRequest body) {
		return todoService.add(body.getName());
	}

	@CrossOrigin
	@PutMapping("/update")
	public TodoResult updateTodo(@RequestBody TodoDTO body) {
		return todoService.update(body);
	}

	@CrossOrigin
	@DeleteMapping("/delete")
	public TodoResult deleteTodo(@RequestParam int id) {
		return todoService.delete(id);
	}

	@CrossOrigin
	@DeleteMapping("/delete-all")
	public TodoResult deleteTodoList() {
		return todoService.delete_all();
	}
}
