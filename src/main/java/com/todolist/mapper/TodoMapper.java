package com.todolist.mapper;

import org.springframework.stereotype.Component;

import com.todolist.dto.TodoDTO;
import com.todolist.entity.TodoEntity;

@Component
public class TodoMapper {
	public TodoEntity toEntity(TodoDTO dto) {
		TodoEntity entity = new TodoEntity();
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		entity.setStatus(dto.getStatus());
		
		return entity;
	}

	public TodoDTO toDTO(TodoEntity entity) {
		TodoDTO dto = new TodoDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setStatus(entity.getStatus());
		
		return dto;

	}
}
